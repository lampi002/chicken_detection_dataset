# chicken_detection_dataset

This repo includes rgb-images of one or multiple chickens and the corresponding bounding box annotations for each image. All images are padded and resized to a resolution of 256 x 256 pixels. The images in this dataset were generated artificially and can be reproduced using generate_dataset.py
